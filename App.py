
import dash

import dash_core_components as dcc
import dash_html_components as html
import plotly.figure_factory as ff

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

class App:
  app = dash.Dash(__name__, external_stylesheets=external_stylesheets)
  df = [
    # dict(Task="Job A", Start='2009-01-01', Finish='2009-02-28'),
    dict(Task="Job B", Start='2009-03-05', Finish='2009-04-15'),
    dict(Task="Job C", Start='2009-02-20', Finish='2009-05-30')
  ]

# from layout import layout
# App.app.layout = layout(App.df)
