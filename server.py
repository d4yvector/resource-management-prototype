
import dash

from App import App

from layout import layout
App.app.layout = layout(App.df)

import callbacks

if __name__ == '__main__':
  App.app.run_server(debug=True)
