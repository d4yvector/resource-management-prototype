
import plotly.figure_factory as ff

from App import App
from dash.dependencies import Input, Output, State

@App.app.callback(
  Output("gantt", "figure"),
  [Input("button", "n_clicks")],
  [State("input", "value")]
)
def update_gannt(n_clicks, value):
  App.df.append(dict(Task=value, Start='2009-01-01', Finish='2009-02-28'))
  return ff.create_gantt(df=App.df, bar_width=0.4)
