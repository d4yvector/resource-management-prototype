
import dash
import dash_core_components as dcc
import dash_html_components as html

import plotly.figure_factory as ff

def layout(df):
  return html.Div(children=[
      html.H1(children="Hello Dash"),

      # gantt
      html.Div(children=[
        dcc.Graph(
          id="gantt",
          figure=ff.create_gantt(df=df, bar_width=0.4)
        )
      ]),
      
      # input
      html.Div(children=[
        dcc.Input(id="input", type="text"), html.Button("Submit", id="button")
      ])
    ])